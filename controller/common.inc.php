<?php
/**
 * ```
 *  _ _ _     __  __ _ _ _            ____  _   _ ____
 * | (_) |__ |  \/  (_) | |_ ___ _ __|  _ \| | | |  _ \
 * | | | '_ \| |\/| | | | __/ _ \ '__| |_) | |_| | |_) |
 * | | | |_) | |  | | | | ||  __/ |  |  __/|  _  |  __/
 * |_|_|_.__/|_|  |_|_|_|_| \___|_|  |_|   |_| |_|_|
 * ```
 *
 */

namespace libMilterPHP;
use libMilterPHP;

/**
 * Common functionality
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package libMilterPHP
 * @subpackage common
 */

// Initialize constants
require_once(dirname(__DIR__) . "/controller/constants.inc.php");

// Initialize the autoloader
require_once(dirname(__DIR__) . "/library/NoumeniaLibMilterPHPAutoloader.inc.php");

// Register the autoloader
spl_autoload_register("libMilterPHP\\NoumeniaLibMilterPHPAutoloader", true);

// Initialize logging
require_once(dirname(__DIR__) . "/controller/loginit.inc.php");

