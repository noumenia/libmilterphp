<?php
/**
 * ```
 *  _ _ _     __  __ _ _ _            ____  _   _ ____
 * | (_) |__ |  \/  (_) | |_ ___ _ __|  _ \| | | |  _ \
 * | | | '_ \| |\/| | | | __/ _ \ '__| |_) | |_| | |_) |
 * | | | |_) | |  | | | | ||  __/ |  |  __/|  _  |  __/
 * |_|_|_.__/|_|  |_|_|_|_| \___|_|  |_|   |_| |_|_|
 * ```
 *
 */

namespace libMilterPHP;

/**
 * Initialize log reporting
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package libMilterPHP
 * @subpackage log
 */

// Parse command line parameters
$libMilterPhpCmdParameters = getopt("qd", array("quiet", "debug"));

// Set destination of log messages
if(
	isset($libMilterPhpCmdParameters['q']) ||
	isset($libMilterPhpCmdParameters['quiet'])
) {

	// Quiet - discard all output
	\Log::setDestination(new \LogDestinationNull());

} elseif(
	isset($libMilterPhpCmdParameters['d']) ||
	isset($libMilterPhpCmdParameters['debug'])
) {

	// Debug output to syslog
	\Log::setDestination(new \LogDestinationSyslog(), LOG_DEBUG);

} else {

	// Display warnings or higher priority messages to syslog
	\Log::setDestination(new \LogDestinationSyslog(), LOG_WARNING);

}

// Clean-up
unset($libMilterPhpCmdParameters);

