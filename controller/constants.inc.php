<?php
/**
 * ```
 *  _ _ _     __  __ _ _ _            ____  _   _ ____
 * | (_) |__ |  \/  (_) | |_ ___ _ __|  _ \| | | |  _ \
 * | | | '_ \| |\/| | | | __/ _ \ '__| |_) | |_| | |_) |
 * | | | |_) | |  | | | | ||  __/ |  |  __/|  _  |  __/
 * |_|_|_.__/|_|  |_|_|_|_| \___|_|  |_|   |_| |_|_|
 * ```
 *
 */

namespace libMilterPHP;

/**
 * Define system-wide constants
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package libMilterPHP
 * @subpackage constants
 */

// Set library version
define("LIBMILTERPHP_VER", "2.0");

// Supported milter protocol version
define("LIBMILTERPHP_PROTOCOL", 2);

// MTA to milter commands (Protocol version 2)
define("SMFIC_ABORT", "A");		// Abort current filter checks
define("SMFIC_BODY", "B");		// Body chunk
define("SMFIC_CONNECT", "C");		// SMTP connection information
define("SMFIC_MACRO", "D");		// Define macros
define("SMFIC_BODYEOB", "E");		// End of body marker
define("SMFIC_HELO", "H");		// HELO/EHLO name
define("SMFIC_HEADER", "L");		// Mail header
define("SMFIC_MAIL", "M");		// MAIL FROM: information
define("SMFIC_EOH", "N");		// End of headers marker
define("SMFIC_OPTNEG", "O");		// Option negotiation
define("SMFIC_RCPT", "R");		// RCPT TO: information
define("SMFIC_QUIT", "Q");		// Quit milter communication

// MTA to milter commands (Protocol version 3)
define("SMFIC_UNKNOWN", "U");		// Not implemented (Any unknown SMTP command)

// MTA to milter commands (Protocol version 4)
define("SMFIC_DATA", "T");		// Not implemented (Headers and body)

// MTA to milter commands (Protocol version 6)
define("SMFIC_QUIT_NC", "K");		// Not implemented (QUIT but new connection follows)

// Reverse MTA to milter commands to their descriptions
define("SMFIC_A", "SMFIC_ABORT - Abort current filter checks");
define("SMFIC_B", "SMFIC_BODY - Body chunk");
define("SMFIC_C", "SMFIC_CONNECT - SMTP connection information");
define("SMFIC_D", "SMFIC_MACRO - Define macros");
define("SMFIC_E", "SMFIC_BODYEOB - End of body marker");
define("SMFIC_H", "SMFIC_HELO - HELO/EHLO name");
define("SMFIC_L", "SMFIC_HEADER - Mail header");
define("SMFIC_M", "SMFIC_MAIL - MAIL FROM: information");
define("SMFIC_N", "SMFIC_EOH - End of headers marker");
define("SMFIC_O", "SMFIC_OPTNEG - Option negotiation");
define("SMFIC_R", "SMFIC_RCPT - RCPT TO: information");
define("SMFIC_Q", "SMFIC_QUIT - Quit milter communication");

// Reverse MTA to milter commands to their descriptions (Protocol version 3)
define("SMFIC_U", "SMFIC_UNKNOWN - Unknown SMTP command");

// Reverse MTA to milter commands to their descriptions (Protocol version 4)
define("SMFIC_T", "SMFIC_DATA - Headers and body");

// Reverse MTA to milter commands to their descriptions (Protocol version 6)
define("SMFIC_K", "SMFIC_QUIT_NC - QUIT but new connection follows");

// Milter actions
define("SMFIF_ADDHDRS", 0x01);		// Add headers
define("SMFIF_CHGBODY", 0x02);		// Change body chunks
define("SMFIF_ADDRCPT", 0x04);		// Add recipients
define("SMFIF_DELRCPT", 0x08);		// Remove recipients
define("SMFIF_CHGHDRS", 0x10);		// Change or delete headers
define("SMFIF_QUARANTINE", 0x20);	// Quarantine message

// Milter actions (Protocol version 6?)
define("SMFIF_CHGFROM", 0x40);		// Replace sender - Not implemented
define("SMFIF_ADDRCPT_PAR", 0x80);	// Add recipients and arguments - Not implemented
define("SMFIF_SETSYMLIST", 0x100);	// Send macro names - Not implemented

// Milter ignored protocol content (Protocol version 2)
define("SMFIP_NOCONNECT", 0x01);	// Suppress SMFIC_CONNECT
define("SMFIP_NOHELO", 0x02);		// Suppress SMFIC_HELO
define("SMFIP_NOMAIL", 0x04);		// Suppress SMFIC_MAIL
define("SMFIP_NORCPT", 0x08);		// Suppress SMFIC_RCPT
define("SMFIP_NOBODY", 0x10);		// Suppress SMFIC_BODY (but will not suppress SMFIC_BODYEOB)
define("SMFIP_NOHDRS", 0x20);		// Suppress SMFIC_HEADER
define("SMFIP_NOEOH", 0x40);		// Suppress SMFIC_EOH

// Milter ignored protocol content (Protocol version 3)
define("SMFIP_NOUNKNOWN", 0x100);

// Milter ignored protocol content (Protocol version 4)
define("SMFIP_NODATA", 0x200);

// Milter ignored protocol content (Protocol version 6)
define("SMFIP_SKIP", 0x400);
define("SMFIP_RCPT_REJ", 0x800);
define("SMFIP_NR_CONN", 0x1000);
define("SMFIP_NR_HELO", 0x2000);
define("SMFIP_NR_MAIL", 0x4000);
define("SMFIP_NR_RCPT", 0x8000);
define("SMFIP_NR_DATA", 0x10000);
define("SMFIP_NR_UNKN", 0x20000);
define("SMFIP_NR_HDR", 0x80);
define("SMFIP_NR_EOH", 0x40000);
define("SMFIP_NR_BODY", 0x80000);
define("SMFIP_HDR_LEADSPC", 0x100000);

// Milter protocol families
define("SMFIA_UNKNOWN", "U");
define("SMFIA_UNIX", "L");
define("SMFIA_INET", "4");
define("SMFIA_INET6", "6");

// Milter response codes			Modification	| Accept/reject	| Asynchronous	| Description
define("SMFIR_ADDRCPT", "+");		//	[X]		| [ ]		| [ ]		| Add recipient
define("SMFIR_DELRCPT", "-");		//	[X]		| [ ]		| [ ]		| Remove recipient
define("SMFIR_ACCEPT", "a");		//	[ ]		| [X]		| [ ]		| Accept
define("SMFIR_REPLBODY", "b");		//	[X]		| [ ]		| [ ]		| Replace body chunk
define("SMFIR_CONTINUE", "c");		//	[ ]		| [X]		| [ ]		| Continue
define("SMFIR_DISCARD", "d");		//	[ ]		| [X]		| [ ]		| Discard
define("SMFIR_ADDHEADER", "h");		//	[X]		| [ ]		| [ ]		| Add header
define("SMFIR_CHGHEADER", "m");		//	[X]		| [ ]		| [ ]		| Change header
define("SMFIR_PROGRESS", "p");		//	[ ]		| [ ]		| [X]		| Progress
define("SMFIR_QUARANTINE", "q");	//	[X]		| [ ]		| [ ]		| Quarantine
define("SMFIR_REJECT", "r");		//	[ ]		| [X]		| [ ]		| Reject
define("SMFIR_TEMPFAIL", "t");		//	[ ]		| [X]		| [ ]		| Tempfail
define("SMFIR_REPLYCODE", "y");		//	[ ]		| [X]		| [ ]		| Reply code

// Milter response codes (Protocol version 6?)
define("SMFIR_ADDRCPT_PAR", "2");	//	[X]		| [ ]		| [ ]		| Add recipient
define("SMFIR_CHGFROM", "e");		//	[X]		| [ ]		| [ ]		| Change envelope FROM sender
define("SMFIR_CONN_FAIL", "f");		//	[ ]		| [X]		| [ ]		| Cause a connection failure
define("SMFIR_INSHEADER", "i");		//	[X]		| [ ]		| [ ]		| Insert header
define("SMFIR_SETSYMLIST", "l");	//			|		|		| Set list of symbols
define("SMFIR_SHUTDOWN", "4");		//			|		|		| 421 Shutdown
define("SMFIR_SKIP", "s");		//			|		|		| Skip

// Reverse response codes to their descriptions
define("SMFIR_+", "SMFIR_ADDRCPT - Add recipient");		// Hack: +/- are not allowed within PHP constants
define("SMFIR_-", "SMFIR_DELRCPT - Remove recipient");
define("SMFIR_A", "SMFIR_ACCEPT - Accept");
define("SMFIR_B", "SMFIR_REPLBODY - Replace body chunk");
define("SMFIR_C", "SMFIR_CONTINUE - Continue");
define("SMFIR_D", "SMFIR_DISCARD - Discard");
define("SMFIR_H", "SMFIR_ADDHEADER - Add header");
define("SMFIR_M", "SMFIR_CHGHEADER - Change header");
define("SMFIR_P", "SMFIR_PROGRESS - Progress");
define("SMFIR_Q", "SMFIR_QUARANTINE - Quarantine");
define("SMFIR_R", "SMFIR_REJECT - Reject");
define("SMFIR_T", "SMFIR_TEMPFAIL - Tempfail");
define("SMFIR_Y", "SMFIR_REPLYCODE - Reply code");
define("SMFIR_O", "SMFIC_OPTNEG - Option negotiation");		// Not named as an SMFIR command, but used as response code

// Reverse response codes to their descriptions (Protocol version 6?)
define("SMFIR_2", "SMFIR_ADDRCPT_PAR - Add recipient");
define("SMFIR_E", "SMFIR_CHGFROM - Change envelope FROM sender");
define("SMFIR_F", "SMFIR_CONN_FAIL - Cause a connection failure");
define("SMFIR_I", "SMFIR_INSHEADER - Insert header");
define("SMFIR_L", "SMFIR_SETSYMLIST - Set list of symbols");
define("SMFIR_4", "SMFIR_SHUTDOWN - 421 Shutdown");
define("SMFIR_S", "SMFIR_SKIP - Skip");

