<?php
/**
 * ```
 *  _ _ _     __  __ _ _ _            ____  _   _ ____
 * | (_) |__ |  \/  (_) | |_ ___ _ __|  _ \| | | |  _ \
 * | | | '_ \| |\/| | | | __/ _ \ '__| |_) | |_| | |_) |
 * | | | |_) | |  | | | | ||  __/ |  |  __/|  _  |  __/
 * |_|_|_.__/|_|  |_|_|_|_| \___|_|  |_|   |_| |_|_|
 * ```
 *
 */

namespace libMilterPHP;

/**
 * Milter class
 *
 * A PHP milter library based on Sendmail milter protocol version 2
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package libMilterPHP
 * @subpackage milter
 */
class Milter extends \SocketManager {

	/**
	 * Supported milter (SMFIF_*) protocol actions in bitmask format
	 * @var int
	 */
	protected $supportedActionsMask = 0;

	/**
	 * Supported milter (SMFIF_*) protocol actions in array format
	 * @var array<int>
	 */
	protected $supportedActions = array();

	/**
	 * Ignored milter content (SMFIP_*) in bitmask format
	 * @var int
	 */
	protected $ignoredContentMask = 0;

	/**
	 * Ignored milter content (SMFIP_*) in array format
	 * @var array<int>
	 */
	protected $ignoredContent = array();

	/**
	 * Protocol action names
	 * @var array<int, string>
	 */
	protected $actionNames = array(
		1	=> "SMFIF_ADDHDRS",
		2	=> "SMFIF_CHGBODY",
		4	=> "SMFIF_ADDRCPT",
		8	=> "SMFIF_DELRCPT",
		16	=> "SMFIF_CHGHDRS",
		32	=> "SMFIF_QUARANTINE"
	);

	/**
	 * Ignored content names
	 * @var array<int, string>
	 */
	protected $ignoredContentNames = array(
		1	=> "SMFIP_NOCONNECT",
		2	=> "SMFIP_NOHELO",
		4	=> "SMFIP_NOMAIL",
		8	=> "SMFIP_NORCPT",
		16	=> "SMFIP_NOBODY",
		32	=> "SMFIP_NOHDRS",
		64	=> "SMFIP_NOEOH"
	);

	/**
	 * Constructor
	 * @param string $effectiveUser Effective process user owner
	 * @param string $effectiveGroup Effective process group owner
	 * @param string $filePid PID file
	 * @param int $processLimit Limit the number of forked processes
	 * @param string $connection Socket file or IPv4/IPv6 address/port string ("unix|socket:/path/to/file.sock" or "inet:port@IP")
	 * @param array<int> $supportedActions Supported actions (SMFIF_*)
	 * @param array<int> $ignoredContent Ignored content (SMFIP_*)
	 * @return void
	 */
	public function __construct(
		string $effectiveUser = "",
		string $effectiveGroup = "",
		string $filePid = "",
		int $processLimit = 1024,
		string $connection = "",
		array $supportedActions = array(),
		array $ignoredContent = array()
	) {

		// Supported actions (bitmask format)
		foreach($supportedActions as &$s)
			$this->addAction($s);

		// Supported actions (array format)
		$this->supportedActions = $supportedActions;

		// Ignored content (bitmask format)
		foreach($ignoredContent as &$i)
			$this->addIgnoredContent($i);

		// Ignored content (array format)
		$this->ignoredContent = $ignoredContent;

		// Clean-up
		unset($s, $i);

		// Call the socket manager constructor
		parent::__construct($effectiveUser, $effectiveGroup, $filePid, $processLimit, $connection);

	}

	/**
	 * Add given action to the supported actions
	 * @param int $action Action constant
	 * @return void
	 */
	private function addAction(int $action): void
	{

		// Report unknown actions
		if(!isset($this->actionNames[$action])) {

			\Log::warning("[Milter: unknown milter action: " . $action . "]");
			return;

		}

		\Log::debug("[Milter: add milter action: " . $this->actionNames[$action] . "]");

		$this->supportedActionsMask |= $action;

	}

	/**
	 * Add given content to the ignored content
	 * @param int $ignore Ignored content
	 * @return void
	 */
	private function addIgnoredContent(int $ignore): void
	{

		// Report unknown ignored content
		if(!isset($this->ignoredContentNames[$ignore])) {

			\Log::warning("[Milter: unknown milter ignored content: " . $ignore . "]");
			return;

		}

		\Log::debug("[Milter: add milter ignored content: " . $this->ignoredContentNames[$ignore] . "]");

		$this->ignoredContentMask |= $ignore;

	}

	/**
	 * Convert binary data into a readable message array
	 * @param string &$binary The binary data
	 * @return array<string, mixed>
	 */
	private function milterBinaryToMessage(string &$binary): array
	{

		/**
		 * Binary message format
		 *
		 * Based on the milter protocol specifications the binary
		 * message has the following format:
		 *
		 * 32bit unsigned integer	= Size of the rest of the message
		 * A single character		= The command character
		 * Data				= Data specific to the command
		 *
		 * For example, here is a binary message in hex:
		 * +----------+---------+--------------------------------+
		 * | size     | command | data...                        |
		 * +----------+---------+----------+----------+----------+
		 * | 0000000d |      4f | 00000006 | 000001ff | 001fffff |
		 * +----------+---------+----------+----------+----------+
		 *
		 * The size is needed when the data can not be terminated by
		 * a character that may be used within the data, like NUL.
		 */

		// Empty message
		$message = array(
			'size'		=> 0,
			'command'	=> "",
			'binary'	=> ""
		);

		// Input validation
		if(empty($binary))
			return $message;

		// Get the size of the message (uint32)
		$size = unpack("N", substr($binary, 0, 4));
		if(
			$size === false ||
			!isset($size[1])
		)
			return $message;

		// Generate the message array

		// Get the size
		$message['size'] = intval($size[1]);

		// Get the command (char)
		$message['command'] = substr($binary, 4, 1);

		// Get the data (chars of uint32 size)
		$message['binary'] = substr($binary, 5, $message['size']);

		try {

			// Reverse the command to its description
			$description = constant("SMFIC_" . $message['command']);
			if(!is_string($description))
				throw new \Error("Unknown SMFIC command");

			\Log::info("[Milter: MTA command: " . $message['command'] . " - " . $description . "]");

			// Clean-up
			unset($description);

		} catch(\Error $e) {

			\Log::info("[Milter: MTA command: " . $message['command'] . "]");

		}

		// Switch between commands
		switch($message['command']) {
			case SMFIC_ABORT:
				// Abort current filter checks
				break;

			case SMFIC_BODY:
				// Body chunk

				// Get a body buffer chunk (char[]), we use a reference
				// so that both variables point to the same content and
				// avoid using double the amount of memory
				$message['buf'] =& $message['binary'];
				break;

			case SMFIC_CONNECT:
				// SMTP connection information

				// Get the hostname (char[])
				$nulPos = strpos($message['binary'], "\0");
				if($nulPos === false)
					break;
				$message['hostname'] = substr($message['binary'], 0, $nulPos);

				// Get the protocol family (char)
				$message['protocolFamily'] = substr($message['binary'], ($nulPos + 1), 1);

				// Optional fields based on the protocol family
				if($message['protocolFamily'] !== SMFIA_UNKNOWN) {

					// Split the data
					$extraData = array(
						substr($message['binary'], ($nulPos + 2), 2),
						substr($message['binary'], ($nulPos + 4))
					);

					/**
					 * Based on the milter protocol specification, the address part
					 * should be NUL terminated, but some MTA implementations do not
					 * append the final NUL.
					 */
					$extraData[1] = str_replace("\0", "", $extraData[1]);

					// Get the port (uint16)
					if(!empty($extraData[0])) {

						$port = unpack("n", $extraData[0]);
						if(
							$port !== false &&
							isset($port[1])
						)
							$message['port'] = intval($port[1]);

						// Clean-up
						unset($port);

					}

					// Get the address (char[])
					if(!empty($extraData[1]))
						$message['address'] = $extraData[1];

					// Clean-up
					unset($extraData);

				}
				break;

			case SMFIC_MACRO:
				// Define macros

				// Get the macro command (char)
				$message['macroCommand'] = substr($message['binary'], 0, 1);

				// Get the macros and their values (NUL separated array)
				$message['macros'] = explode("\0", substr($message['binary'], 1));
				break;

			case SMFIC_BODYEOB:
				// End of body marker
				break;

			case SMFIC_HELO:
				// HELO/EHLO name

				// Get the helo (char[])
				$nulPos = strpos($message['binary'], "\0");
				if($nulPos === false)
					break;
				$message['helo'] = substr($message['binary'], 0, $nulPos);
				break;

			case SMFIC_HEADER:
				// Mail header

				// Split the data by the NUL character
				$extraData = explode("\0", $message['binary']);

				// Get the name (char[])
				if(isset($extraData[0]))
					$message['name'] = $extraData[0];

				// Get the value (char[])
				if(isset($extraData[1]))
					$message['value'] = $extraData[1];
				break;

			case SMFIC_MAIL:
				// MAIL FROM: information

				// Get the sender and the ESMTP arguments (NUL separated array)
				$message['args'] = explode("\0", $message['binary']);
				break;

			case SMFIC_EOH:
				// End of headers marker
				break;

			case SMFIC_OPTNEG:
				// Option negotiation

				// Protocol version (uint32)
				$rc = unpack("N", substr($message['binary'], 0, 4));
				if(
					$rc === false ||
					!isset($rc[1])
				)
					\Log::error("[Milter: error parsing the protocol version]");
				else
					$message['protocolVersion'] = intval($rc[1]);

				// Actions (uint32)
				$rc = unpack("N", substr($message['binary'], 4, 4));
				if(
					$rc === false ||
					!isset($rc[1])
				)
					\Log::error("[Milter: error parsing the actions]");
				else
					$message['allowedActions'] = intval($rc[1]);

				// Protocol content (uint32)
				$rc = unpack("N", substr($message['binary'], 8, 4));
				if(
					$rc === false ||
					!isset($rc[1])
				)
					\Log::error("[Milter: error parsing the protocol content]");
				else
					$message['protocolContent'] = intval($rc[1]);
				break;

			case SMFIC_RCPT:
				// RCPT TO: information

				// Get the sender and the ESMTP arguments (NUL separated array)
				$message['args'] = explode("\0", $message['binary']);
				break;

			case SMFIC_QUIT:
				// Quit milter communication
				break;

			default:
				\Log::warning("[Milter: unknown command: " . $message['command'] . "]");
				break;
		}

		return $message;

	}

	/**
	 * Convert the message array into binary data
	 * @param array{command: string, data: array<string, int|string>} &$reply Message array
	 * @return string
	 */
	private function messageToMilterBinary(array &$reply): string
	{

		try {

			// Reverse the response code to its description
			$description = constant("SMFIR_" . strtoupper($reply['command']));
			if(!is_string($description))
				throw new \Error("Unknown SMFIR command");

			\Log::info("[Milter: reply command: " . $reply['command'] . " - " . $description . "]");

			// Clean-up
			unset($description);

		} catch(\Error $e) {

			\Log::info("[Milter: reply command: " . $reply['command'] . "]");

		}

		// Set the command (char)
		$binary = $reply['command'];

		// If there is data to append...
		if(sizeof($reply['data']) > 0) {

			// Loop data
			foreach($reply['data'] as $type => &$d) {

				// Switch types
				switch($type) {
					// Type char, NUL terminated
					case "rcpt":
					case "name":
					case "value":
					case "reason":
					case "text":
						$binary .= iconv("UTF-8", "ASCII", strval($d)) . "\0";
						break;

					// Type char
					case "buf":
					case "smtpcode":
						$binary .= $d;
						break;

					// Type char, forced space
					case "space":
						$binary .= " ";
						break;

					// Type uint32
					case "index":
					case "version":
					case "actions":
					case "protocol":
						$binary .= pack("N", $d);
						break;

					// Unknown type
					default:
						\Log::error("[Milter: unknown reply type: " . $type . "]");
						break;
				}

			}

		}

		return $binary;

	}

	/**
	 * Transmit a milter reply to the MTA
	 * @param array{command: string, data: array<string, int|string>} &$reply Message array
	 * @return bool
	 */
	public function reply(array &$reply): bool
	{

		// Convert message array into binary data
		$mtaReply = $this->messageToMilterBinary($reply);

		// Send data to the MTA
		return $this->socketWriteBySize($mtaReply);

	}

	/**
	 * Process as a child, called when the parent process forks
	 * @return bool
	 */
	public function processChild(): bool
	{

		\Log::debug("[Milter: process forked child process]");

		// To infinity and beyond!
		while(true) {

			// Read data from the MTA
			$mtaData = $this->socketReadBySize();

			// Convert binary data into a readable message array
			$message = $this->milterBinaryToMessage($mtaData);

			// Switch between commands
			switch($message['command']) {
				case SMFIC_ABORT:
					// Abort current filter checks

					// Call implementation function
					if(method_exists($this, "smficAbort"))
						$this->smficAbort($message);
					else
						\Log::warning("[Milter: function `smficAbort()` not implemented]");
					break;

				case SMFIC_BODY:
					// Body chunk - use SMFIP_NOBODY to suppress

					// Call implementation function
					if(method_exists($this, "smficBody"))
						$this->smficBody($message);
					else
						\Log::warning("[Milter: function `smficBody()` not implemented]");
					break;

				case SMFIC_CONNECT:
					// SMTP connection information - use SMFIP_NOCONNECT to suppress

					// Call implementation function
					if(method_exists($this, "smficConnect"))
						$this->smficConnect($message);
					else
						\Log::warning("[Milter: function `smficConnect()` not implemented]");
					break;

				case SMFIC_MACRO:
					// Define macros

					// Call implementation function
					if(method_exists($this, "smficMacro"))
						$this->smficMacro($message);
					else
						\Log::warning("[Milter: function `smficMacro()` not implemented]");

					// The MTA does not expect a response to a SMFIC_MACRO command
					break;

				case SMFIC_BODYEOB:
					// End of body marker

					// Call implementation function
					if(method_exists($this, "smficBodyeob"))
						$this->smficBodyeob($message);
					else
						\Log::warning("[Milter: function `smficBodyeob()` not implemented]");
					break;

				case SMFIC_HELO:
					// HELO/EHLO name - use SMFIP_NOHELO to suppress

					// Call implementation function
					if(method_exists($this, "smficHelo"))
						$this->smficHelo($message);
					else
						\Log::warning("[Milter: function `smficHelo()` not implemented]");
					break;

				case SMFIC_HEADER:
					// Mail header - use SMFIP_NOHDRS to suppress

					// Call implementation function
					if(method_exists($this, "smficHeader"))
						$this->smficHeader($message);
					else
						\Log::warning("[Milter: function `smficHeader()` not implemented]");
					break;

				case SMFIC_MAIL:
					// MAIL FROM: information - use SMFIP_NOMAIL to suppress

					// Call implementation function
					if(method_exists($this, "smficMail"))
						$this->smficMail($message);
					else
						\Log::warning("[Milter: function `smficMail()` not implemented]");
					break;

				case SMFIC_EOH:
					// End of headers marker - use SMFIP_NOEOH to suppress

					// Call implementation function
					if(method_exists($this, "smficEoh"))
						$this->smficEoh($message);
					else
						\Log::warning("[Milter: function `smficEoh()` not implemented]");
					break;

				case SMFIC_OPTNEG:
					// Option negotiation

					// Reply with our own SMFIC_OPTNEG command
					$reply = array(

						// Set the command (char)
						'command'	=> SMFIC_OPTNEG,

						// Set the data (chars of uint32 size)
						'data'		=> array(
							'version'	=> LIBMILTERPHP_PROTOCOL,
							'actions'	=> $this->supportedActionsMask,
							'protocol'	=> $this->ignoredContentMask
						)

					);

					$this->reply($reply);
					break;

				case SMFIC_RCPT:
					// RCPT TO: information - use SMFIP_NORCPT to suppress

					// Call implementation function
					if(method_exists($this, "smficRcpt"))
						$this->smficRcpt($message);
					else
						\Log::warning("[Milter: function `smficRcpt()` not implemented]");
					break;

				case SMFIC_QUIT:
					// Quit milter communication

					// Call implementation function
					if(method_exists($this, "smficQuit"))
						$this->smficQuit($message);
					else
						\Log::warning("[Milter: function `smficQuit()` not implemented]");
					break 2;

				default:
					// Quit milter communication
					break 2;
			}

		}

		return true;

	}

}

