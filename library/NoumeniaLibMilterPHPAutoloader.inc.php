<?php
/**
 * ```
 *  _ _ _     __  __ _ _ _            ____  _   _ ____
 * | (_) |__ |  \/  (_) | |_ ___ _ __|  _ \| | | |  _ \
 * | | | '_ \| |\/| | | | __/ _ \ '__| |_) | |_| | |_) |
 * | | | |_) | |  | | | | ||  __/ |  |  __/|  _  |  __/
 * |_|_|_.__/|_|  |_|_|_|_| \___|_|  |_|   |_| |_|_|
 * ```
 *
 */

namespace libMilterPHP;

/**
 * Autoload function for file classes
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package libMilterPHP
 * @subpackage autoloader
 *
 * @param string $className Class name as requested by PHP
 * @return void
 */
function NoumeniaLibMilterPHPAutoloader($className)
{

	// Remove namespace and add extension
	$classFile = str_replace("libMilterPHP\\", "", $className) . ".inc.php";

	// Load file from library
	$filePath = dirname(__DIR__) . "/library/" . $classFile;
	if(is_file($filePath)) {

		require_once($filePath);
		return;

	}

	// Load file from interface
	$filePath = dirname(__DIR__) . "/interface/" . $classFile;
	if(is_file($filePath)) {

		require_once($filePath);
		return;

	}

}

