# Changelog libMilterPHP

## Development

## Release 2.0

- Store and compare PID as an integer
- Improvements for strict type checking
- Define Option negotiation as a response code

## Release 1.9

- Use a reference to avoid using double memory
- Describe response codes
- Reverse response codes to their descriptions
- Minor documentation updates
- Reverse MTA commands to their descriptions
- Describe MTA to milter commands

## Release 1.8

- Optimize socket read functions
- Set maxMessageSize as a protected variable

## Release 1.7

- Create the PID directory if it does not already exist
- Make the processList private (again)

## Release 1.6

- Implement the Log facility parameter for syslog

## Release 1.5

- Get the status of the child process that just ended

## Release 1.4

- Correct effective user setting
- Enable socket reuse address mode

## Release 1.3

- Disable slow debug messages
- Improve socket read size calculation
- Create a copr repository with RPM packages
- Update README.md file
- Use a proper buffer to read large messages
- The process limit should not be less than 1
- Avoid getopt conflicts with milter application
- Support for q/quiet command-line parameters
- Documentation updates for the SocketManager
- Make external objects more portable
- Separate the Log from the library namespace
- Separate the DaemonManager from the library namespace
- Separate the SocketManager from the library namespace

## Release 1.2

- Allow empty body for POST requests
- Alternate between HTTP stream and socket functions
- Fix incorrect SMFIC_CONNECT address/port messages
- Improve handling of child signals
- Ignore signals from sub processes

## Release 1.1

- Add missing functions to sample milter
- Improve socket handling on error
- Implement a forked process limit
- Add a remote connect function to the SocketManager
- Silence extra warnings from socket shutdown/close
- Set socket send/receive timeout to 10 seconds
- Shutdown socket before socket close
- Disable socket linger mode
- Remove repeating debug messages
- Remove extra socket_select calls
- Update protocol constants
- Disable blacklog connections
- Remove the socket file only on valid sockets
- Add a composer.json file
- Cosmetic changes to documentation

## Release 1.0

- Initial public release

